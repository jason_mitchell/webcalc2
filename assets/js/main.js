import {Terminal} from "./terminal.js"

var term = Terminal();
console.log(math);

term.onExecute = function(string)
{
    try
    {
        term.line("output", math.eval(string))
    }
    catch(e)
    {
        console.error(e);
        term.line("error",e);
    }
}

document.onkeydown = () => {
    $("html,body").scrollTop(0);
    $("#input-line").focus() //focus on input line
};

document.onclick = (event) =>
{
    let target = $(event.target); //the
    let opened = $("#menu-toggle").hasClass("is-active") //if the sidebar is open
    let sidebar = target.parents("div#sidebar-wrapper").length //if what was clicked was the sidebar
    let toggler = target.is("#menu-toggle") || target.parents("#menu-toggle").length //if what was clicked was the sidebar
    let mobile = Math.max(document.documentElement.clientWidth, window.innerWidth || 0) < 767;
    
    //if what was clicked was not sidebar in mobile, or if the toggler was clicked, toggle
    if ((mobile && opened && !sidebar) || toggler)
    {
         var toggled = $("#wrapper").toggleClass("toggled");
         $("#menu-toggle").toggleClass("is-active", toggled)
    }
};

//enable tooltips everywhere
$(function () {$('[data-toggle="tooltip"]').tooltip()})

$("#btn-clear").click(() => {term.clear()});